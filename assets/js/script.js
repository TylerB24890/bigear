/**
 * @author  Tyler Bailey
 * @package Big Ears 2015 - Pyxl
 */

// Global variables
isTouch = !!("undefined" != typeof document.documentElement.ontouchstart);
$window = $(window);
ww = $window.width();
wh = $window.height();

$(document).ready(function() {
    $('#image-carousel').slick({
          nextArrow: '#slider-next',
          prevArrow: '#slider-prev'
    });
    
    // If NOT on a mobile device
    if(!isTouch && ww > 891) {
        $('#top-banner').parallax('50%', .7);    
    }
});


// Parallax
jQuery.fn.parallax = function(xpos, speedFactor, outerHeight) {
    var $this = $(this);
    var getHeight;
    var firstTop;
    var paddingTop = 0;

    // Elements starting position
    $this.each(function(){
        firstTop = $this.offset().top;
    });

    if (outerHeight) {
        getHeight = function(jqo) {
            return jqo.outerHeight(true);
        };
    } else {
        getHeight = function(jqo) {
            return jqo.height();
        };
    }

    // Setup defaults if arguments aren't specified
    if (arguments.length < 1 || xpos === null) xpos = "50%";
    if (arguments.length < 2 || speedFactor === null) speedFactor = 0.1;
    if (arguments.length < 3 || outerHeight === null) outerHeight = true;

    // Function to be called whenever the window is scrolled or resized
    function update(){
        var pos = $window.scrollTop();

        $this.each(function(){
            var $element = jQuery(this);
            var top = $element.offset().top;
            var height = getHeight($element);

            // Check if totally above or totally below viewport
            if (top + height < pos || top > pos + wh) {
                return;
            }

            $this.css('backgroundPosition', xpos + " " + Math.round((firstTop + pos) * speedFactor) + "px");
        });
    }

    $window.bind('scroll', update).resize(update);
    update();
};